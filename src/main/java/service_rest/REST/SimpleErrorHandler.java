package service_rest.REST;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class SimpleErrorHandler implements ErrorHandler {

	/* ATTRIBUT */
	private boolean errorOccured = false;

	/* COMMANDES */
    public void warning(SAXParseException e) throws SAXException {
        System.out.println(e.getMessage());
        errorOccured = true;
    }

    public void error(SAXParseException e) throws SAXException {
        System.out.println(e.getMessage());
        errorOccured = true;
    }

    public void fatalError(SAXParseException e) throws SAXException {
        System.out.println(e.getMessage());
        errorOccured = true;
    }

    /* REQUETES */
    public boolean hasError() {
    	return errorOccured;
    }
}