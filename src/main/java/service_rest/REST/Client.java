package service_rest.REST;

import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBSource;
import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.http.HTTPBinding;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import tp.model.Amount;
import tp.model.CmpsdAgt;
import tp.model.CmpsdIdt;
import tp.model.CmpsdNmd;
import tp.model.CmpsdTx;
import tp.model.Document;
import tp.model.SEPA;

/**
 * The entry point of this project.
 */
public class Client extends Application {

	// ATTRIBUTES
	private RestSepaBeans beans;
	private javax.xml.ws.Service service;
	private JAXBContext jc;

	private static final QName qname = new QName("", "");
	private static final String url = "http://localhost:8080/REST";

	public Client() {
		try {
			jc = JAXBContext.newInstance(Document.class, Amount.class, CmpsdAgt.class, CmpsdIdt.class, CmpsdNmd.class,
					CmpsdTx.class, SEPA.class, Document.DrctDbtTxInf.class, Document.Stats.class, Document.Resume.class,
					Document.Trx.class);
		} catch (JAXBException je) {
			System.out.println("Cannot create JAXBContext " + je);
		}
	}

	/**
	 * @param url
	 *            : url du serveur associée au service demandé pour la requête
	 * @param method
	 *            : méthode HTTP utilisé pour la requête
	 * @param body
	 *            : corps envoyé via la méthode HTTP (lorsque la méthode
	 *            correspond logiquement à PUT ou POST)
	 * @return Une Source correspondant à la réponse du service demandé.
	 *
	 *         Méthode utilisée pour tous les appels au service depuis le client
	 *         pour exécuter toutes les commandes désirées du TP.
	 */
	public Source callServiceOnServer(String url, String method, JAXBSource body) {
		service = javax.xml.ws.Service.create(qname);
		service.addPort(qname, HTTPBinding.HTTP_BINDING, url);
		Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
		Map<String, Object> requestContext = dispatcher.getRequestContext();
		requestContext.put(MessageContext.HTTP_REQUEST_METHOD, method);
		Source result = dispatcher.invoke(body);
		return result;
	}

	// ---------------------------------- AFFICHAGE RÉSULTAT -------------------------------------------
	/**
	 * @param s
	 * La Source dont on souhaite l'affichage XML.
	 * Affiche la source passée en paramètre en XML.
	 */
	public void printSource(Source s) {
		try {
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();
			transformer.transform(s, new StreamResult(System.out));
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void stats() throws JAXBException {
		printSource(callServiceOnServer(url + "/stats", "GET", null));
	}

	public void trx(String n) throws JAXBException {
		printSource(callServiceOnServer(url + "/trx/" + n, "GET", null));
	}

	/*
	 * public Document trx(String n) throws JAXBException { Source source =
	 * callServiceOnServer(url + "/trx/" + n, "GET", null); Document document =
	 * (Document) this.jc.createUnmarshaller().unmarshal(source); Source s = new
	 * JAXBSource(jc, document); printSource(s); return document; }
	 */
	public void depot(Document.DrctDbtTxInf transaction) throws JAXBException {
		printSource(callServiceOnServer(url + "/depot", "POST", new JAXBSource(jc, transaction)));
	}

	public void resume() throws JAXBException {
		printSource(callServiceOnServer(url + "/resume", "GET", null));
	}

	// COMMANDS
	@Override
	public void start(Stage stage) throws Exception {
		beans = RestSepaBeans.getInstance();
		beans.setWindow(stage);

		BorderPane borderpane = new BorderPane();
		Scene scene = new Scene(borderpane); {
			FXMLLoader fxmlLoader;
			fxmlLoader = new FXMLLoader(getClass().getResource("/ressources/Menu.fxml"));
			Node menu = fxmlLoader.load();
			MenuController menuController = fxmlLoader.getController();
			menuController.setUmlReverseBeans(beans);
			menuController.setScene(scene);
			beans.setMenuController(menuController);
			borderpane.setLeft(menu);
		}

		scene.getStylesheets().add("/ressources/Styles.css");
		setActionToQuitApplication(stage);
		stage.setTitle("service REST SEPA");
		stage.setScene(scene);
		stage.show();
		stage.setResizable(false);
	}

	static void setActionToQuitApplication(Stage stage) {
		// Show alert when you click in the red boutton close
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				System.exit(0);
			}
		});
	}

	public static void main(String[] args) {

		 try { Client client = new Client();
			 System.out.println("\nSTATS\n");
			 client.stats();
			 System.out.println("\n\nRESUME\n");
			 client.resume();
			 System.out.println("\n");
			 /*client.trx("KT0001");
			 Document.DrctDbtTxInf drc1 = new Document.DrctDbtTxInf();
			 drc1.setInstdAmt(new Amount(new Amount.InstdAmt(1)));
			 drc1.setPmtInf("KT0004");
			 drc1.setRmtInf("Transaction4");
			 client.depot(drc1); client.stats();*/
		 } catch (Exception e) {
		 	e.printStackTrace();
		 }
		launch(args);
	}
}
