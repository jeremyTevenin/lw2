package service_rest.REST;

import java.io.File;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.stage.Window;

/**
 * Diverse elements used by the project.
 */
public class RestSepaBeans {

	// ATTRIBUTES
	private static RestSepaBeans INSTANCE;

    /**
     * Object property on file to detect a file change.
     */
    private final ObjectProperty<File> fileProperty;

    /**
     * The menu controller of the application.
     */
    private MenuController menuController;

    /**
     * The Stage that will contain all pane.
     */
    private Window window;

    // CONSTRUCTORS
    /**
     * Constructor of the umlReverseBeans.
     * It will contain all data that the project deserve to work correctly
     */
    private RestSepaBeans() {
        fileProperty = new SimpleObjectProperty<>();
    }

    /**
     * Getter of Menu Controller.
     * @return MenuController
     */
    public MenuController getMenuController() {
    	return menuController;
    }

    // COMMANDS
    /**
     * Getter of the save file.
     * @return File
     */
    public File getFile() {
        return fileProperty.get();
    }

    /**
     * Setter of the save file.
     * @param f The file that will serve for the save
     */
    public void setFile(File f) {
        fileProperty.setValue(f);
    }

    /**
     * Getter of the file property.
     * @return ObjectProperty<File>
     */
    public ObjectProperty<File> getFileProperty() {
        return fileProperty;
    }

    /**
     * Getter of the stage of the application.
     * @return Window
     */
    public Window getStage() {
        return window;
    }

    /**
     * Setter of the window.
     * @param stage The stage of the application.
     */
    public void setWindow(Window stage) {
        this.window = stage;
    }

    // STATIC METHODS
    /**
     * Singleton of UmlReverseBeans.
     * @return UmlReverseBeans
     */
    public static RestSepaBeans getInstance() {
    	if (INSTANCE == null) {
    		INSTANCE = new RestSepaBeans();
    	}
    	return INSTANCE;
    }

	public void setMenuController(MenuController menuController) {

	}
}
