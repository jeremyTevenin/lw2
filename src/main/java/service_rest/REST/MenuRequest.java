package service_rest.REST;

import java.util.HashMap;
import java.util.Map;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;


public class MenuRequest extends StackPane {

	// ATTRIBUTES
    /**
     * Map with name of request as key and the button request as value.
     */
    private Map<String, Button> entityMap;

    /**
     * Variable that give access to the different part of the view.
     */
    private RestSepaBeans beans;

    /**
     * Constructor that create an empty scrollPane.
     *
     * @param beans the beans of the application
     */

    public MenuRequest(RestSepaBeans beans) {
        this.beans = beans;
        entityMap = new HashMap<>();
    }

    // COMMANDS
    /**
     * getter of entityMap that contain all entity of the project.
     * @return entityMap
     */
    public Map<String, Button> getEntityMap() {
        return entityMap;
    }
}

