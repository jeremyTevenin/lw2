package service_rest.REST;

import org.springframework.web.bind.annotation.RequestBody;
import tp.model.Amount;
import tp.model.Document;
import tp.model.SEPA;

import javax.ws.rs.*;
import javax.xml.bind.JAXBException;
import java.math.BigDecimal;

@Path("/")
public class MyServiceTP {

    private Document document = new Document();

    public MyServiceTP() {

    }

    /**
     * GET method stats
     */
    @GET
    @Path("/stats")
    @Produces("application/xml")
    public Document.Stats getStats() throws JAXBException {
        return document.getStats();
    }

    /**
     * GET method resume
     */
    @GET
    @Path("/resume")
    @Produces("application/xml")
    public Document.Resume getResume() throws JAXBException {
        return document.getResume();
    }

    /**
     * GET method trx
     */
    @GET
    @Path("/trx/n")
    @Consumes({"text/xml", "application/xml", "application/*+xml", "application/json" })
    @Produces("application/xml")
    public Document.Trx getTrx(@PathParam("n") String n) throws JAXBException {
        document.setNameTransaction(n);
        return document.getTrx();
    }

    /**
     * POST method depot
     */
    @POST
    @Path("/depot")
    @Consumes({"text/xml", "application/xml", "application/*+xml"})
    @Produces("application/xml")
    public void postDepot(@RequestBody Document.DrctDbtTxInf transaction) throws JAXBException {
        document.setDepot(transaction);
    }
}
