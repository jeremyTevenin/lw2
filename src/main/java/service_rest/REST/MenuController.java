package service_rest.REST;

import java.io.File;
import java.io.IOException;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.print.PrinterJob;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

public class MenuController {

	// ATRIBUTES
	/**
	 * Menu item that allow to create a new class diagram.
	 */
	@FXML
	private Button resume;
	/**
	 * Menu item that allow to create a new sequence diagram.
	 */
	@FXML
	private Button stats;
	/**
	 * Menu item that allow to create a new package diagram.
	 */
	@FXML
	private Button trx;
	/**
	 * Menu item that allow to export a diagram to plant uml.
	 */
	@FXML
	private Button depot;

	@FXML
	private Pane information;

	private TextArea textarea;

	private Button chooseFile;

	private Button validate;

	private Button deleteFile;

	private TextField field;

	private Client client;

	/**
	 * The stage of the application.
	 */
	private Window stage;

	/**
	 * The beans of the application.
	 */
	private RestSepaBeans beans;

	private Scene scene;

	// COMMANDS
	/**
	 * Set UmlReverseBeans.
	 *
	 * @param beans
	 *            the beans of the application
	 */
	public void setUmlReverseBeans(RestSepaBeans beans) {
		client = new Client();
		this.beans = beans;

		textarea = new TextArea("\nBienvenue sur notre service REST SEPA\n" + "\n" + "François KOPKA\n"
				+ "Jérémy TEVENIN\n" + "\n" + "30 avril 2017");
		textarea.setEditable(false);

		information.getChildren().add(textarea);

		createComposant();
		createController();
	}

	public void setScene(Scene scene) {
		this.scene = scene;
	}

	private void setActionToQuitApplication(Stage stage) {
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				stage.close();
			}
		});
	}

	/**
	 * Request /resume
	 */
	public void onActionResume() {
		hideButton();
		textarea.setVisible(true);

		Stage stage = new Stage();
		stage.setResizable(false);

		Pane pane = new Pane();
		Scene scene = new Scene(pane); {
			TextArea textSynthese = new TextArea();
			textSynthese.setEditable(false);
			ScrollPane sp = new ScrollPane(textSynthese);
			pane.getChildren().add(sp);
		}

		scene.getStylesheets().add("/ressources/Styles.css");
		setActionToQuitApplication(stage);
		stage.setTitle("Resume");
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * Request /stats
	 */
	public void onActionStats() throws JAXBException {
		hideButton();
		textarea.setVisible(true);

		Stage stage = new Stage();
		stage.setResizable(false);

		Pane pane = new Pane();
		Scene scene = new Scene(pane);
		{
			TextArea textSynthese = new TextArea();
			textSynthese.setEditable(false);
			ScrollPane sp = new ScrollPane(textSynthese);
			pane.getChildren().add(sp);
		}

		scene.getStylesheets().add("/ressources/Styles.css");
		setActionToQuitApplication(stage);
		stage.setTitle("Stats");
		stage.setScene(scene);
		stage.show();
	}

	/*
	 * Request /depot
	 */
	public void onActionDepot() {
		textarea.setVisible(false);

		Pane gp = new Pane(); {
			chooseFile.setVisible(true);
			chooseFile.setTranslateX(10);
			chooseFile.setTranslateY(50);
			gp.getChildren().add(chooseFile);

			validate.setVisible(true);
			validate.setDisable(true);
			validate.setTranslateX(80);
			validate.setTranslateY(50);
			gp.getChildren().add(validate);

			deleteFile.setVisible(true);
			validate.setDisable(true);
			deleteFile.setTranslateX(150);
			deleteFile.setTranslateY(50);
			gp.getChildren().add(deleteFile);

			field.setVisible(true);
			field.setEditable(false);
			field.setPrefSize(300, 20);
			field.setTranslateX(10);
			field.setTranslateY(10);
			gp.getChildren().add(field);
		}

		gp.setPadding(new Insets(10));
		information.getChildren().add(gp);
	}

	/**
	 * Request /trx/n
	 */
	public void onActionTrx() {
		hideButton();
	}

	private void createController() {
		chooseFile.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(final ActionEvent e) {
				FileChooser fileChooser = new FileChooser();
				FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
				fileChooser.getExtensionFilters().add(extFilter);
				fileChooser.setTitle("Choisir un fichier xml");

				File file = fileChooser.showOpenDialog(stage);

				if (file != null) {
					deleteFile.setDisable(false);
					field.setText(file.getAbsolutePath());
					validate.setDisable(false);
				} else {
					deleteFile.setDisable(true);
					field.setText("");
					validate.setDisable(true);
				}
			}
		});

		validate.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				try {
					should_validate_with_DOM();
				} catch (SAXException e) {
					e.printStackTrace();
				} catch (ParserConfigurationException e) {
					e.printStackTrace();
				} catch (IOException e) {
					System.out.println("Problème lors de l'ouverture du fichier : " + e);
				}
			}
		});

		deleteFile.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				field.setText("");
				validate.setDisable(true);
				deleteFile.setDisable(true);
			}
		});
	}

	private void createComposant() {
		chooseFile = new Button("Choisir");
		chooseFile.setVisible(false);

		field = new TextField();
		field.setText("fichier xml à importer");
		field.setEditable(false);
		field.setVisible(false);

		validate = new Button("Valider");
		validate.setVisible(false);

		deleteFile = new Button("Supprimer");
		deleteFile.setDisable(true);
		deleteFile.setVisible(false);
	}

	private void hideButton() {
		chooseFile.setVisible(false);
		field.setVisible(false);
		validate.setVisible(false);
		deleteFile.setVisible(false);
	}

	private void should_validate_with_DOM() throws SAXException, ParserConfigurationException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(false);
		factory.setNamespaceAware(true);

		SchemaFactory schemaFactory =
		    SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");

		factory.setSchema(schemaFactory.newSchema(
		    new Source[] {new StreamSource("validation.xsd")}));

		DocumentBuilder builder = factory.newDocumentBuilder();

		builder.setErrorHandler(new SimpleErrorHandler());

		Document document = builder.parse(new InputSource("test_validation_bon.xml"));
	}

	/**
	 * Close Application Menu Alert.
	 */
	public void close() {
		Alert dialogW = new Alert(AlertType.WARNING, "Voulez vous sauvegarder avant de quitter l'application ?",
				ButtonType.CANCEL, ButtonType.NO, ButtonType.YES);
		dialogW.setTitle("Attention");
		dialogW.setHeaderText("Attention, votre travail n'est pas sauvegardé.");
		dialogW.showAndWait();
		if (dialogW.getResult() != ButtonType.CANCEL) {
			if (dialogW.getResult() == ButtonType.YES) {
			}
			System.exit(0);
		}
	}

}
