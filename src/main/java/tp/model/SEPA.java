package tp.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "SEPA")
public class SEPA {
	
	// ATTRIBUTS
	String transaction;
	int montant;
	
	// CONSTRUCTEURS
	public SEPA(String transaction, int montant) {
		this.transaction = transaction;
		this. montant = montant;
	}
	
	public SEPA() {
		montant = 0;
		transaction = "";
	}
	
	// REQUETES
	public int getMontant() {
		return montant;
	}
	
	public String getTransaction() {
		return transaction;
	}
	
	// COMMANDES
	@XmlElement
	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}
	
	@XmlElement
	public void setMontant (int montant) {
		this. montant = montant;
	}
	
}