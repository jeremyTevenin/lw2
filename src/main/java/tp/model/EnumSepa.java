package tp.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Classe Java pour Sepa.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * <p>
 * <pre>
 * &lt;simpleType name="EnumSepa">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SEPA"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 *
 */
@XmlType(name = "EnumSepa")
@XmlEnum
public class EnumSepa {
    //SEPA;
/*
    public String value() {
        return name();
    }

    public static EnumSepa fromValue(String v) {
        return valueOf(v);
    }*/
}
